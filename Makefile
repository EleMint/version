install: build
	@mkdir -p ~/.local/bin
	@cp ./bin/version ~/.local/bin

install-path: build
	@echo "Installing to ${INSTALL_PATH}"
	@cp ./bin/version ${INSTALL_PATH}

build:
	@go build -o ./bin/version ./cmd/main.go

clean:
	@rm ./bin/version

clean-install:
	@rm ~/.local/bin/version

clean-path:
	@rm ${INSTALL_PATH}/version
