package version

import (
	"errors"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

const DEFAULT_VERSION = "0.0.0"

const VERSION_FILE_PATH = "./VERSION"
const VERSION_ERR_NOT_EXIST = "error: version file (%s) does not exist"
const VERSION_ERR_IS_DIR = "error: version file (%s) is a directory"
const VERSION_ERR_STAT = "error: failed to read version file (%s) information\n%s"
const VERSION_ERR_OPEN = "error: failed to open version file (%s)\n%s"
const VERSION_ERR_READ = "error: failed to read version file (%s)\n%s"
const VERSION_ERR_WRITE = "error: failed to write version file (%s)\n%s"
const VERSION_ERR_TRUNCATE = "error: failed to truncate version file (%s)\n%s"
const VERSION_ERR_PARSE = "error: failed to parse version file (%s)"

type Version struct {
	file *os.File

	major int
	minor int
	patch int
}

func (v *Version) stat() error {
	info, err := os.Stat(VERSION_FILE_PATH)
	if err != nil {
		if errors.Is(err, os.ErrNotExist) {
			return nil
		}

		return fmt.Errorf(VERSION_ERR_STAT, VERSION_FILE_PATH, err)
	}

	if info.IsDir() {
		return fmt.Errorf(VERSION_ERR_IS_DIR, VERSION_FILE_PATH)
	}

	return nil
}

func (v *Version) open() error {
	err := v.stat()
	if err != nil {
		return err
	}

	file, err := os.OpenFile(VERSION_FILE_PATH, os.O_CREATE|os.O_RDWR, 0666)
	if err != nil {
		return fmt.Errorf(VERSION_ERR_OPEN, VERSION_FILE_PATH, err)
	}

	v.file = file

	return nil
}

func (v *Version) close() {
	v.file.Close()
	v.file = nil
}

func (v *Version) read(buffer []byte) (int, error) {
	n, err := v.file.Read(buffer)
	if err != nil {
		if errors.Is(err, io.EOF) {
			return 0, nil
		}

		return 0, fmt.Errorf(VERSION_ERR_READ, VERSION_FILE_PATH, err)
	}

	return n, nil
}

func (v *Version) truncate() error {
	err := v.file.Truncate(0)
	if err != nil {
		return fmt.Errorf(VERSION_ERR_TRUNCATE, VERSION_FILE_PATH, err)
	}

	_, err = v.file.Seek(0, 0)
	if err != nil {
		return fmt.Errorf(VERSION_ERR_TRUNCATE, VERSION_FILE_PATH, err)
	}

	return nil
}

func (v *Version) write(buffer []byte) (int, error) {
	n, err := v.file.Write(buffer)
	if err != nil {
		return 0, fmt.Errorf(VERSION_ERR_WRITE, VERSION_FILE_PATH, err)
	}

	return n, nil
}

func (v *Version) parse() error {
	buffer := make([]byte, 128)

	n, err := v.read(buffer)
	if err != nil {
		return err
	}

	s := string(buffer[:n])
	if len(s) == 0 {
		s = DEFAULT_VERSION
	}

	split := strings.Split(s, ".")

	switch len(split) {
	case 1:
		split = append(split, "0", "0")
	case 2:
		split = append(split, "0")
	}

	maj, err := strconv.Atoi(split[0])
	if err != nil {
		return fmt.Errorf(VERSION_ERR_PARSE, VERSION_FILE_PATH)
	}

	min, err := strconv.Atoi(split[1])
	if err != nil {
		return fmt.Errorf(VERSION_ERR_PARSE, VERSION_FILE_PATH)
	}

	pat, err := strconv.Atoi(split[2])
	if err != nil {
		return fmt.Errorf(VERSION_ERR_PARSE, VERSION_FILE_PATH)
	}

	v.major = maj
	v.minor = min
	v.patch = pat

	return nil
}

func (v *Version) put() error {
	s := fmt.Sprintf("%d.%d.%d", v.major, v.minor, v.patch)

	err := v.truncate()
	if err != nil {
		return err
	}

	buffer := []byte(s)

	_, err = v.write(buffer)
	if err != nil {
		return err
	}

	return nil
}

func Increment(semver SemVer) (*Version, error) {
	v := new(Version)

	err := v.open()
	if err != nil {
		return nil, err
	}

	defer v.close()

	err = v.parse()
	if err != nil {
		return nil, err
	}

	switch semver {
	case MAJOR:
		v.major++
		v.minor = 0
		v.patch = 0
	case MINOR:
		v.minor++
		v.patch = 0
	case PATCH:
		v.patch++
	case NIL:
		v.major = 0
		v.minor = 0
		v.patch = 0
	}

	err = v.put()
	if err != nil {
		return nil, err
	}

	return v, nil
}

func Show() error {
	v := new(Version)

	err := v.open()
	if err != nil {
		return err
	}

	defer v.close()

	err = v.parse()
	if err != nil {
		return err
	}

	fmt.Printf("%d.%d.%d\n", v.major, v.minor, v.patch)

	return nil
}
