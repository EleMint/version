package version

type SemVer int

const (
	NIL SemVer = iota
	MAJOR
	MINOR
	PATCH
)

func (s SemVer) String() string {
	switch s {
	case MAJOR:
		return "major"
	case MINOR:
		return "minor"
	case PATCH:
		return "patch"
	case NIL:
		return "nil"
	default:
		return "nil"
	}
}

func ParseSemVer(s string) SemVer {
	switch s {
	case "major":
		return MAJOR
	case "minor":
		return MINOR
	case "patch":
		return PATCH
	case "nil":
		return NIL
	default:
		return NIL
	}
}

const SEMVER_ERR_INVALID = "error: invalid semver option (%s)\nexpected: major | minor | patch"
