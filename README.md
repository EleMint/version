# Version - Semantic Versioning

## Dependencies
  - Golang - [https://go.dev/dl](https://go.dev/dl)
  - GNU Make
    - Linux - [https://www.gnu.org/software/make/](https://www.gnu.org/software/make/)
    - OSX - [Homebrew](https://brew.sh/)
      ```bash
      brew install make
      ```
    - Windows
      1. [Chocolatey](https://chocolatey.org/install)
      2. [Direct Install](http://gnuwin32.sourceforge.net/packages/make.htm)
      3. Window Subsystem for Linux (WSL/WSL2) - [ref](https://docs.microsoft.com/en-us/windows/wsl/install)

## Clone the repo

   ```bash
   git clone https://gitlab.com/EleMint/version
   # OR
   git clone git@gitlab.com:EleMint/version
   ```

## Project Usage

   - Build the project locally (`./bin/version`)
     ```bash
     make build
     ```
   - Install the project (`~/.local/bin`)
     ```bash
     make
     # OR
     make install
     ```
   - Install the project (`$INSTALL_PATH`)
     ```bash
     export INSTALL_PATH=~/.local/bin  # or preferred installation path
     make install-path
     ```

## Versioning

   - `version`
     - displays current version from `./VERSION`
     - if `./VERSION` does not exsist
       1. file is created
       2. version set to `0.0.0`

   - `version [major | minor | patch]`
     - increments specified part of version (`+`)
       - `major` -> `+.x.x`
       - `minor` -> `x.+.x`
       - `patch` -> `x.x.+`

   - `version -v` or `version --version`
     - displays current source/installed version

   - `version -h` or `version --help`
     - displays this usage information

## Future Iterations
  - [ ] Add command to update to new `version` installation
  - [ ] Commit changes to `git` repository (or `--no-commit`)
  - [ ] Add flag to decrement version (`--dec`)
