package main

import (
	"flag"
	"fmt"
	"os"

	version "gitlab.com/EleMint/version/src"
)

func main() {
	h := flag.Bool("h", false, "prints this usage message")
	help := flag.Bool("help", false, "prints this usage message")

	ver := flag.Bool("v", false, "prints version information")
	vers := flag.Bool("version", false, "prints version information")

	flag.Parse()

	if *h || *help {
		usage()
		os.Exit(1)
	}

	if *ver || *vers {
		versionInfo()
		os.Exit(1)
	}

	if len(os.Args) < 2 {
		err := version.Show()
		if err != nil {
			fatalError(err)
		}
		return
	}

	semver := version.ParseSemVer(os.Args[1])
	if semver == version.NIL {
		fatalString(version.SEMVER_ERR_INVALID, os.Args[1])
	}

	_, err := version.Increment(semver)
	if err != nil {
		fatalError(err)
	}
}

func usage() {
	fmt.Println("Version Usage:")
	fmt.Println("\tversion <options> [major | minor | patch]")
	fmt.Println("")
	fmt.Println("\toptions:")
	fmt.Println("\t\t-h --help\tprints this usage message")
	fmt.Println("\t\t-v --version\tprints version information")

}

func versionInfo() {
	f, err := os.Open(version.VERSION_FILE_PATH)
	if err != nil {
		panic(err)
	}

	defer f.Close()

	buffer := make([]byte, 128)

	n1, err := f.Read(buffer)
	if err != nil {
		panic(err)
	}

	fmt.Println(string(buffer[:n1]))
}

func fatalString(format string, a ...interface{}) {
	fmt.Printf(format, a...)
	fmt.Println("")
	usage()
	fmt.Println("")
	os.Exit(1)
}

func fatalError(err error) {
	fmt.Println(err)
	fmt.Println("")
	usage()
	fmt.Println("")
	os.Exit(1)
}
